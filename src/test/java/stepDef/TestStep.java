package stepDef;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageActions.TestActions;

public class TestStep {
			
	
		TestActions obj = new TestActions();
		
		@BeforeTest
		public void user_launches_browser() {
			
			obj.launchBrowser();
		}
		
		@Test(priority=1)
		public void user_goes_to_url(){
			
			obj.goToUrl();
		} 
		
		@Test(priority=2)
		public void user_searches_any_term() throws InterruptedException {
			
			obj.search();
		}
		
		@AfterTest
		public void user_closes_browser() {
			
			obj.closeBrowser();
		}
}
