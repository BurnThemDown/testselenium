package pageLocators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TestLocators {

	WebDriver driver = null;
	
	@FindBy(xpath="//*[@name='q']")
	public WebElement searchBox;
	
	public TestLocators(WebDriver driver) { 
		
        this.driver = driver; 
	}
}
