package pageActions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import pageLocators.TestLocators;

public class TestActions {
	
	WebDriver driver = null;
	
	public void launchBrowser() {
		// TODO Auto-generated method stub
	    System.setProperty("webdriver.chrome.driver", "E:/Selenium requirements/chromedriver_win32/chromedriver.exe");
	    System.out.println(">>Launching browser");
	    driver = new ChromeDriver();
	}
		
    
	
	
	public void goToUrl() {
		// TODO Auto-generated method stub	
		System.out.println("Browser is launched..");
		
		System.out.println(">>Going to google URL");
		driver.get("https://www.google.com/");
		System.out.println("Google URL launched..");
		
		System.out.println(">>Maximizing browser");
		driver.manage().window().maximize();
		System.out.println("Browser maximized..");
	}

	public void search() throws InterruptedException {
		// TODO Auto-generated method stub
		
		TestLocators locators = PageFactory.initElements(driver, TestLocators.class);
		
		System.out.println(">>Searching facebook");
		locators.searchBox.click();
		locators.searchBox.sendKeys("Facebook");
		locators.searchBox.sendKeys(Keys.RETURN);
		System.out.println("Facebook has been searched");
		Thread.sleep(500);
	}

	public void closeBrowser() {
		// TODO Auto-generated method stub
		System.out.println(">>Closing session");
		driver.quit();
		System.out.println("Session closed..");
	}
}
